from ib_insync import *
import ib_insync as ibn

## Connecting to TWS
ib = IB()
try:
    ib.connect('127.0.0.1' , 7497 , clientId=40)
except Exception as e:
    print("Error connecting TWS, Error: " , e)


## Fetching Account Name
account = ib.managedAccounts()[0]



## ReqAccountUpdates
# This is called at startup - no need to call again.
        
# Request account and portfolio values of the account
# and keep updated. Returns when both account values and portfolio
# are filled.



## Printing Account Updates
print("Account Updates")
Account_Updates = util.df(ib.accountValues())
print(Account_Updates.head())
print("\n\n")


## Printing Portfolio
print("Portfolio")
Portfolio = util.df(ib.portfolio())
print(Portfolio.head())
print("\n\n")

## Printing Account Summary
print("Account Summary")
Account_Summary = util.df(ib.accountSummary())
print(Account_Summary.head())
print("\n\n")