
## THREADING Simple
import threading
import time

exitFlag = 0

class myThread (threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
   def run(self):
      print ("Starting " + self.name)
      Funct()
#      print_time(self.name, 5, self.counter)
      print ("Exiting " + self.name)

def print_time(threadName, counter, delay):
   while counter:
      if exitFlag:
         threadName.exit()
      time.sleep(delay)
      print( "%s: %s" % (threadName, time.ctime(time.time())))
      counter -= 1
      
def Funct():
    time.sleep(50)
    print("EXECUTED")

# Create new threads
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

# Start new Threads
thread1.start()
thread2.start()

print ("Exiting Main Thread")



#thread1.start()
#
#thread1.isAlive()
#
#thread1.isDaemon()






## THREADING WITH QUEUE
import queue
import threading
import time

exitFlag = 0

class myThread (threading.Thread):
   def __init__(self, threadID, name, q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.q = q
   def run(self):
      print ("Starting " + self.name)
      process_data(self.name, self.q)
      print ("Exiting " + self.name)

def process_data(threadName, q):
   while not exitFlag:
     queueLock.acquire()
     if not workQueue.empty():
        data = q.get()
        queueLock.release()
        print ("%s processing %s" % (threadName, data))
     else:
        queueLock.release()
     time.sleep(5)

threadList = ["Thread-1", "Thread-2", "Thread-3"]
nameList = ["One", "Two", "Three", "Four", "Five"]
queueLock = threading.Lock()
workQueue = queue.Queue(10)
threads = []
threadID = 1

# Create new threads
for tName in threadList:
   thread = myThread(threadID, tName, workQueue)
   thread.start()
   threads.append(thread)
   threadID += 1

# Fill the queue
queueLock.acquire()
for word in nameList:
   workQueue.put(word)
queueLock.release()

# Wait for queue to empty
while not workQueue.empty():
   pass

# Notify threads it's time to exit
exitFlag = 1

# Wait for all threads to complete
for t in threads:
   t.join()
print( "Exiting Main Thread")






## THREADING WITH QUIT
import threading
import time


def Funct1(arg):
    t1 = threading.currentThread()
    while getattr(t1, "do_run", True):
        print ("Executing Task-1")
        time.sleep(1)
    print("Stopping Task-1")
    
def Funct2(arg):
    t2 = threading.currentThread()
    while getattr(t2, "do_run", True):
        print ("Executing Task-2")
        time.sleep(1)
    print("Stopping Task-2")


def main():
    t1 = threading.Thread(target=Funct1, args=("task",))
    t1.start()
    
    t2 = threading.Thread(target=Funct2, args=("task",))
    t2.start()
    Stop_Call = input()
    if(Stop_Call == 'quit'):
        t1.do_run = False
        t2.do_run = False

if __name__ == "__main__":
    main()